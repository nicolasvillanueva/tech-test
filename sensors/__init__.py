import random
import time


def _event_type():
    event_types = ["nominal", "info", "warning", "error", "critical"]
    return random.choices(event_types, cum_weights=[60, 24, 10, 5, 1], k=1)[0]


def do_work():
    time.sleep(random.randint(2, 10))


class Sensor:
    def __init__(self, sensor_id: str):
        self.sensor_id = sensor_id
        print(f"Created sensor: {self.sensor_id}")

    @property
    def state(self):
        return {
            "id": self.sensor_id,
            "event": {
                "type": _event_type(),
                "readings": [
                    random.randint(0, 100),
                    random.randint(0, 100),
                    random.randint(0, 100)
                ]
            },
            "timestamp": int(time.time())
        }
